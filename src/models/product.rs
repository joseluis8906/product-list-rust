use std;
use std::io;
use mongodb::{bson, doc};
use mongodb::ThreadedClient;
use mongodb::db::ThreadedDatabase;
use crate::lib;

#[derive(Debug)]
pub struct Model {
  pub name: String,
  pub price: i32,
}

/*
impl Model {
  pub fn to_bson(&self) -> mongodb::ordered::OrderedDocument {
    doc! { 
      "name": self.name.to_owned(),
      "price": self.price.to_owned(),
    }
  }
}
*/

pub fn find_one(name: String) -> Result<std::option::Option<mongodb::ordered::OrderedDocument>, io::Error> {
  let client = lib::mongo::establish_connection();
  let collection = client.db("jarvis").collection("products");
  let response_document = collection.find_one(Some(doc! { "name" => name }), None).ok().expect("Failed to execute find.");
  Ok(response_document)
}

pub fn find_all() -> Result<std::option::Option<mongodb::cursor::Cursor>, io::Error>{
  let client = lib::mongo::establish_connection();
  let collection = client.db("jarvis").collection("products");
  let response_list = collection.find(None, None).ok().expect("Failed to execute find.");
  Ok(Some(response_list))
}


#[derive(Serialize, Deserialize, Debug)]
pub struct ProductMongo {
  //pub _id: bson::oid::ObjectId,
  pub name: String,
  pub price: i32,
}